import React from "react";
import { useNavigate, Navigate } from "react-router-dom";
export default function Success() {
    const navigate = useNavigate();
    const data = JSON.parse(localStorage.getItem('obj'))
  const logOut = ()=>{
    data.is_authenticated = false;
    localStorage.setItem('obj',JSON.stringify(data));
    navigate('/signin', {replace:true})
  } 
  return (
    <>
    {data.is_authenticated ? 
      <div>
        <h1>success to log in </h1>
        <button onClick={logOut}>log out</button>
      </div>
      :
        <Navigate to='/signin' replace={true}/>
      }
      
    </>
  );
}
