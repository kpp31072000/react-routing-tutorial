import React from "react";
import "antd/dist/antd.css";
import { Button, Form, Input, Radio, Row, Col } from "antd";

const FormDisabledDemo = () => {
  const onFinish = (values) => {
    console.log(values);
  };
  return (
    <>
      <div className="container">
        <Form onFinish={onFinish}>
          <Row >
            <Col span={5}>
              <Form.Item label="Name" name="name">
                <Input />
              </Form.Item>
            </Col>
            <Col span={1}>
            </Col>
            <Col span={5}>
              <Form.Item label="Surname" name="surname">
                <Input />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={5}>
              <Form.Item label="Date Of Birth" name="dob">
                <Input type="date" max="2022-06-23" />
              </Form.Item>
            </Col>
            <Col span={1}>
            </Col>
            <Col span={5}>
              <Form.Item label="Gender" name="gender">
                <Radio.Group>
                  <Radio value="male" style={{ paddingLeft: "5px" }}>
                    {" "}
                    Male{" "}
                  </Radio>
                  <Radio value="female" style={{ paddingLeft: "5px" }}>
                    {" "}
                    Female{" "}
                  </Radio>
                  <Radio value="other" style={{ paddingLeft: "5px" }}>
                    {" "}
                    Other{" "}
                  </Radio>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={5}>
              <Form.Item label="Gmail" name="gmail">
                <Input type="email" />
              </Form.Item>
            </Col>
            <Col span={1}>
            </Col>
            <Col span={5}>
              <Form.Item label="Number" name="number">
                <input
                  type="text"
                  pattern="[0-9]{10}"
                  style={{
                    width: "100%",
                  }}
                />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={11}>
              <Form.Item label="Address" name="address">
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col offset={5}>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit Data
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </div>
    </>
  );
};

export default FormDisabledDemo;
