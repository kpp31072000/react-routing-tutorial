import "antd/dist/antd.css";
import { Outlet, Link, useNavigate } from "react-router-dom";
// import  {Link}  from "react-router-dom";
import { LockOutlined, UserOutlined } from "@ant-design/icons";
import { Button, Checkbox, Form, Input, Row, Col } from "antd";
import React from "react";
// import Success from "./Component/Success.js";

const App = (varr) => {
  const navigate = useNavigate();
  const handle = () => {
    const username = document.getElementById("username").value;
    const password = document.getElementById("password").value;

    const par = JSON.parse(localStorage.getItem("obj"));
    // console.log(par)

    if (par == null) {
      console.log("wrong user name");
    } else if (username === par.name && password === par.password ) {
      console.log("success");
      navigate('/success', {replace:true})
      par.is_authenticated = true;
     localStorage.setItem("obj", JSON.stringify(par))

    } else if (username !== par.name ) {
      document.getElementById("input").innerHTML = "Please input valid  UserID"

    } else if (password !== par.password) {
      document.getElementById("input").innerHTML = "Please enter valid  Password"

    } else {
      console.log("some error ");
    }
  
  };

  return (

    <div className="container2">
      <div id="input" style={{color:"red"}}></div>
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{
          remember: true,
        }}
      >
        <Row>
          <Col span={9} offset={7}>
            <Form.Item
            id="name"
              name="username"
              rules={[
                {
                  required: true,
                  message: "Please input your Username!",
                },
              ]}
            >
              <Input
                prefix={<UserOutlined className="site-form-item-icon" />}
                id="username"
                name="username"
                placeholder="Username"
              />
            </Form.Item>
          </Col>
          <Col span={1}></Col>
        </Row>
        <Row>
          <Col span={9} offset={7}>
            <Form.Item
            id="pass"
              name="password"
              rules={[
                {
                  required: true,
                  message: "Please input your Password!",
                },
              ]}
            >
              <Input
                prefix={<LockOutlined className="site-form-item-icon" />}
                name="password"
                type="password"
                id="password"
                placeholder="Password"
              />
            </Form.Item>
          </Col>
          <Col span={1}></Col>
        </Row>

        <Row>
          <Col span={9} offset={7}>
            <Form.Item>
              <Form.Item name="remember" valuePropName="checked" noStyle>
                <Checkbox>Remember me</Checkbox>
              </Form.Item>

              <a className="login-form-forgot" href="">
                Forgot password
              </a>
            </Form.Item>
          </Col>
          <Col span={1}></Col>
        </Row>
        <Row>
          <Col span={9} offset={7}>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
                onClick={handle}
              >
                log in
              </Button>
              Or <Link to="/signup">Sign Up</Link>
            </Form.Item>
            <Outlet />
          </Col>
          <Col span={1}></Col>
        </Row>
      </Form>
    </div>
  );
};

export default App;
