import React from "react";
import { Outlet, Link } from "react-router-dom";
export default function Layout() {
  return (
    <>
      <div className="container2">
        <button className="btn">
          {" "}
          <Link to="/signup">Sign Up</Link>
        </button>
        <button className="btn">
          {" "}
          <Link to="/signin">Sign In</Link>
        </button>
        <button className="btn">
          {" "}
          <Link to="/">Form</Link>
        </button>
      </div>
      <div>
        <Outlet />
      </div>
    </>
  );
}
