import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import "./App.css";
import Form from "./Component/Form.js";
import Layout from "./Component/Layout.js";
import SignUp from "./Component/SignUp.js";
import SignIn from "./Component/SignIn.js";
import NoPage from "./Component/NoPage.js";
import Success from "./Component/Success.js";

function App() {
  return (
    <div className="App">
      {/* <Form/> */}
      {/* <SignUp/> */}
      {/* <SignIn /> */}

      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Layout />}>
            <Route index element={<Form />} />
            <Route path="signup" element={<SignUp />} />
            <Route path="signin" element={<SignIn />} />
            <Route path="*" element={<NoPage />} />
          </Route>
          <Route  path="success"  element={<Success/>} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
